#!/usr/bin/env python
import os
import sys
import argparse
import uvicorn


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('command', help="Available commands:\nrunserver")
    parser.add_argument('host', nargs='?', default='127.0.0.1', type=str,
                        help="Which host ip to run the server on (default 127.0.0.1)")
    parser.add_argument('--port', '-p', dest="port", default=8000, type=int,
                        help="Which port to run the server against (default 8000)")
    args = parser.parse_args()
    if args.command == 'runserver':
        from config.app import app
        uvicorn.run(app, host=args.host, port=args.port)

if __name__ == "__main__":
    os.environ.setdefault("STARLETTE_SETTINGS_MODULE", "config.settings.local")
    current_path = os.path.dirname(os.path.abspath(__file__))
    sys.path.append(os.path.join(current_path, "emotive_backend"))
    main()