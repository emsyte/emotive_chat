from starkit.conf import settings

templates = settings.TEMPLATES

async def homepage(request):
    template = 'index.html'
    context = {'request': request,
               'emotive_api': settings.EMOTIVE_API }
    return templates.TemplateResponse(template, context)
