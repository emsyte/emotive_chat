import typing
import jwt
import logging
import aiohttp
from dataclasses import dataclass
from starkit.conf import settings

log = logging.getLogger(__name__)


@dataclass
class EmoResponse:
    status: int
    json: dict


class EmoApi:

    TOKEN_OBTAIN = '/auth/token/obtain/'
    REGISTRATION_KEY = '/schedule-registration-key/{key}/'

    def __init__(self, base_url: str=None) -> None:
        self.base_url = base_url or settings.EMOTIVE_API

    def api_url(self, path: str) -> str:
        return f'{self.base_url}{path}'

    async def post(self, path: str, json: dict) ->  EmoResponse:
        async with aiohttp.ClientSession() as session:
            async with session.post(self.api_url(path), json=json) as response:
                return EmoResponse(status=response.status, json=await response.json())

    async def get(self, path: str) ->  EmoResponse:
        async with aiohttp.ClientSession() as session:
            async with session.get(self.api_url(path)) as response:
                return EmoResponse(status=response.status, json=await response.json())

    async def authenticate(self, username: str, password: str) -> bool:
        response = await self.post(self.TOKEN_OBTAIN, {'username': username, 'password': password})
        if response.status != 200:
            if response.status == 401:
                log.info(f"emoapi login failed: {username}")
            else:
                log.error(f"emoapi unexpected status {response.status} for {self.api_url(self.TOKEN_OBTAIN)}")
            return False
        else:
            self.access_token = response.json.get('access')
            self.refresh_token = response.json.get('refresh')
            try:
                self.auth_payload = jwt.decode(str(self.access_token),
                                               settings.JWT['SIGNING_KEY'],
                                               algorithms=[settings.JWT['ALGORITHM']])
                return True
            except jwt.exceptions.InvalidSignatureError as e:
                log.error(str(e))
        return False

    async def lookup_registration(self, key: str) -> typing.Optional[typing.Dict]:
        response = await self.get(self.REGISTRATION_KEY.format(key=key))
        if response.status != 200:
            if response.status == 404:
                log.info(f"registration key {key} not found")
            else:
                log.error(f"emoapi unexpected status {response.status} for"
                          " {self.api_url(self.REGISTRATION_KEY.format(key=key))}")
        else:
            return response.json
