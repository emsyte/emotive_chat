import typing
import asyncio
import aioredis
import logging
from starlette.routing import Router
from starlette.endpoints import HTTPEndpoint, WebSocketEndpoint
from starlette.websockets import WebSocket

from starkit.conf import settings
from starkit.auth import ChatAuthBackend
from chatlette.endpoints import ChatWebSocketEndpoint

log = logging.getLogger(__name__)

templates = settings.TEMPLATES
app = Router()


@app.route('/channel/{channel}', name='channel')
class User(HTTPEndpoint):
    async def get(self, request):
        template = 'chat/channel.html'
        context = {'request': request,
                   'emotive_api': settings.EMOTIVE_API,
                   'channel': request.path_params['channel'],
                   }
        return templates.TemplateResponse(template, context)


@app.websocket_route("/chat/{channel}", name='chatws')
class Chat(ChatWebSocketEndpoint):

    require_authenticated = False

    async def action_new_user_login(self, websocket: WebSocket, content: typing.Any) -> None:
        auth = ChatAuthBackend()
        username = content.get('username')
        password = content.get('password')
        if username is not None and password is not None:
            auth_result = await auth.user_login(username, password)
            if auth_result is not None:
                self.scope["auth"], self.scope["user"] = auth_result
                self.user = self.scope["user"]
                credentials, self.user = auth_result
                await self.publish_all({'action': 'new_user',
                                        'chatname': self.user.display_name,
                                        })
                return
        await self.force_close(websocket)

    async def action_new_key_login(self, websocket: WebSocket, content: typing.Any) -> None:
        auth = ChatAuthBackend()
        registration_key = content.get('key')
        if registration_key is not None:
            auth_result = await auth.key_registration(registration_key)
            if auth_result is not None:
                self.scope["auth"], self.scope["user"] = auth_result
                self.user = self.scope["user"]
                await self.publish_all({'action': 'new_user',
                                        'chatname': self.user.display_name,
                                        })
                return
        await self.force_close(websocket)

    async def action_set_chatname(self, websocket: WebSocket, content: typing.Any) -> None:
        self.user.chatname = content['chatname']
        await self.publish_all({'action': 'set_chatname',
                                'chatname': self.user.display_name,
                                'username': self.user.username,
                                })

    async def action_send_message(self, websocket: WebSocket, content: typing.Any) -> None:
        await self.publish_all({'action': 'send_message',
                                'chatname': self.user.chatname,
                                'username': self.user.username,
                                'content': content
                                })

    async def reader(self, channel: aioredis.pubsub.Channel, websocket: WebSocket) -> None:
        while await channel.wait_message():
            message = await channel.get_json()
            log.debug(f'redis channel reader: {message}')

            if self.allow_message(message, websocket):
                await websocket.send_json(message)

    def allow_message(self, message: typing.Any, websocket: WebSocket):
        content = message.get('content', {})
        chatname = message.get('chatname')
        to = content.get('to', None)

        if to:
            # The sender and the receiver will both be able to see this message
            return to == self.chatname or chatname == self.chatname
        return True
