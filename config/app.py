import logging
from starlette.applications import Starlette
from starlette.routing import Mount, Route, Router
from starlette.staticfiles import StaticFiles
from starlette.middleware.authentication import AuthenticationMiddleware
from starkit.auth import ChatAuthBackend
from starkit.conf import settings

from pages.home import homepage
from pages.error import not_found, server_error
from chat.app import app as chat_app

log = logging.getLogger(__name__)

app = Starlette(debug=settings.DEBUG)
app.add_middleware(AuthenticationMiddleware, backend=ChatAuthBackend())

app.mount('/static', StaticFiles(directory='static'), name='static')
app.add_exception_handler(404, not_found)
app.add_exception_handler(500, server_error)
app.mount('', Router([
    Route('/', endpoint=homepage, methods=['GET']),
    Mount('/chat', app=chat_app, name='chat')
]))

@app.on_event('startup')
async def startup():
    app.state.settings = settings
