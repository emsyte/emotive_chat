import logging
import logging.config
from starlette.config import Config
from starlette.templating import Jinja2Templates

config = Config(".env")

DEBUG = config('DEBUG', cast=bool, default=False)
TEMPLATES = Jinja2Templates(directory='templates')
EMOTIVE_API = 'https://api.stage2.emotive.tailoredlabs.io/api/v1'
REDIS_URL = 'redis://localhost'

LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
    },
    'handlers': {
        'default': {
            'level': 'INFO',
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',  # Default is stderr
        },
        'debug': {
            'level': 'DEBUG',
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',  # Default is stderr
        },
    },
    'loggers': {
        '': {  # root logger
            'handlers': ['default'],
            'level': 'INFO',
            'propagate': False
        },
        'chat': {
            'handlers': ['debug'],
            'level': 'DEBUG',
            'propagate': False
        },
        'chat': {
            'handlers': ['debug'],
            'level': 'DEBUG',
            'propagate': False
        },
        'chatlette': {
            'handlers': ['debug'],
            'level': 'DEBUG',
            'propagate': False
        },
        '__main__': {  # if __name__ == '__main__'
            'handlers': ['default'],
            'level': 'DEBUG',
            'propagate': False
        },
    }
}

logging.config.dictConfig(LOGGING_CONFIG)

# django-rest-framework-simplejwt: https://github.com/davesque/django-rest-framework-simplejwt
JWT = {
    'ALGORITHM': 'HS256',
    'SIGNING_KEY': config('JWT_SIGNING_KEY',
                       default='vaQr40sISJIV5TrtSYkzdZSLxiu23mJ96gxPGyHVDCoG34A0GLL28XmXshZ9JGQp',
                       ),

    'AUTH_HEADER_TYPES': ('Bearer',),
}