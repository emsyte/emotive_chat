import os
from importlib import import_module


class Settings:

    def __init__(self, settings_module):

        self.SETTINGS_MODULE = settings_module

        mod = import_module(self.SETTINGS_MODULE)

        for setting in dir(mod):
            if setting.isupper():
                setting_value = getattr(mod, setting)

                setattr(self, setting, setting_value)

ENVIRONMENT_VARIABLE = "STARLETTE_SETTINGS_MODULE"
settings = Settings(os.environ.get(ENVIRONMENT_VARIABLE))