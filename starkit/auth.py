import typing
import logging
from urllib.parse import unquote
import base64
import binascii

from starlette.requests import HTTPConnection
from starlette.authentication import (
    AuthenticationBackend, AuthenticationError, SimpleUser, UnauthenticatedUser,
    AuthCredentials
)

from chat.emoapi import EmoApi

log = logging.getLogger(__name__)

class ChatUser(SimpleUser):
    def __init__(self, username: str, chatname: str=None, emoapi: EmoApi=None) -> None:
        super().__init__(username)
        self.chatname = chatname
        self.emoapi = emoapi

    @property
    def display_name(self) -> str:
        if self.chatname:
            return self.chatname
        return self.username.split('@')[0]

    def is_authenticated(self) -> bool:
        return self.emoapi is not None


class ChatAuthBackend(AuthenticationBackend):
    async def authenticate(
        self, request: HTTPConnection
    ) -> typing.Optional[typing.Tuple["AuthCredentials", "BaseUser"]]:
        auth = request.headers.get('Authorization') or request.cookies.get('Authorization')
        log.info(f"Cookies: {request.cookies}")
        if auth is not None and auth:
            return await self.authorization_login(auth)

        key = request.headers.get('event-registration-key') or request.cookies.get('event-registration-key')
        if key is not None and key:
            return await self.key_registration(key)

    async def authorization_login(self, auth: str) -> typing.Optional[typing.Tuple["AuthCredentials", "BaseUser"]]:
        auth = unquote(auth)
        try:
            scheme, credentials = auth.split()
            if scheme.lower() != 'basic':
                return None
            decoded = base64.b64decode(credentials).decode("ascii")
        except (ValueError, UnicodeDecodeError, binascii.Error) as exc:
            raise AuthenticationError('Invalid basic auth credentials')
        username, _, password = decoded.partition(":")
        return await self.user_login(username, password)

    async def user_login(self, username: str, password: str) -> typing.Optional[typing.Tuple["AuthCredentials", "BaseUser"]]:
        emoapi = EmoApi()
        if await emoapi.authenticate(username, password):
            return AuthCredentials(["authenticated"]), ChatUser(username, emoapi=emoapi)

        return None

    async def key_registration(self, key: str) -> typing.Optional[typing.Tuple["AuthCredentials", "BaseUser"]]:
        emoapi = EmoApi()
        event_registration = await emoapi.lookup_registration(key)
        if event_registration is not None:
            return AuthCredentials(["registered"]), ChatUser(event_registration['email'],
                                                             chatname=event_registration['name'], emoapi=emoapi)

        return None
